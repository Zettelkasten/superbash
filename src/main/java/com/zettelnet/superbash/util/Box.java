package com.zettelnet.superbash.util;

public final class Box implements Sizable {

	public static final Box EMPTY = new Box(0, 0);

	private final double width, height;

	public Box(double width, double height) {
		this.width = width;
		this.height = height;
	}

	@Override
	public Box getBoundry() {
		return this;
	}

	public double getWidth() {
		return width;
	}

	public int width() {
		return (int) width;
	}

	public double getHeight() {
		return height;
	}

	public int height() {
		return (int) height;
	}

	public boolean overlapps(Location loc, Location other, Box otherBox) {
		if (loc.getArena() != other.getArena()) {
			return false;
		}
		return loc.getX() <= other.getX() + otherBox.getWidth() && loc.getX() + getWidth() > other.getX()
				&& loc.getY() <= other.getY() + otherBox.getHeight() && getHeight() + loc.getY() > other.getY();
	}

	public boolean onBounds(Location loc, Location other) {
		if (loc.getArena() != other.getArena()) {
			return false;
		}
		return overlapps(loc, other, Box.EMPTY);
	}

	public Box scale(double scale) {
		return new Box(this.width * scale, this.height * scale);
	}

	public Vector toVector() {
		return new Vector(width, height);
	}

	@Override
	public String toString() {
		return "Box [width=" + width + ", height=" + height + "]";
	}
}
