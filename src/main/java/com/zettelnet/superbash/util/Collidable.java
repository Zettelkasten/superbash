package com.zettelnet.superbash.util;

public interface Collidable extends Positionable {

	public Box getCollisionBox();
}
