package com.zettelnet.superbash.util;

public final class Vector {

	public static final Vector NULL = new Vector(0, 0);

	private final double x, y;

	public Vector(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public Vector add(Vector other) {
		return new Vector(this.x + other.x, this.y + other.y);
	}

	public Vector multiply(double factor) {
		return new Vector(x * factor, y * factor);
	}

	public double getX() {
		return x;
	}

	public int x() {
		return (int) x;
	}

	public double getY() {
		return y;
	}

	public int y() {
		return (int) y;
	}

	public Box toBox() {
		return new Box(x, y);
	}

	@Override
	public String toString() {
		return "Vector [x=" + x + ", y=" + y + "]";
	}
}
