package com.zettelnet.superbash.util;

public interface Tickable {

	public void tick();
}
