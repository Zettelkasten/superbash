package com.zettelnet.superbash.util;

public interface Sizable {

	public Box getBoundry();
}
