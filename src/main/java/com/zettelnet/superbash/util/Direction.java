package com.zettelnet.superbash.util;

public enum Direction {

	LEFT(new Vector(-1, 0)), RIGHT(new Vector(-1, 0));

	private final Vector unitVector;

	Direction(Vector unitVector) {
		this.unitVector = unitVector;
	}

	public Vector getUnitVector() {
		return unitVector;
	}
	
	public Vector getVector(double size) {
		return unitVector.multiply(size);
	}
}
