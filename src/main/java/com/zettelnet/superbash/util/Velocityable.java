package com.zettelnet.superbash.util;


public interface Velocityable {

	public Vector getVelocity();
}
