package com.zettelnet.superbash.util;

import com.zettelnet.superbash.arena.Arena;

public final class Location implements Positionable, Cloneable {

	private Arena arena;
	private double x, y;

	public Location(Arena arena, double x, double y) {
		this.arena = arena;
		this.x = x;
		this.y = y;
	}

	@Override
	public Location getLocation() {
		return this;
	}

	public Arena getArena() {
		return arena;
	}

	public Arena setArena(Arena arena) {
		this.arena = arena;
		return arena;
	}

	public double getX() {
		return x;
	}

	public double setX(double x) {
		this.x = x;
		return x;
	}

	public int x() {
		return (int) x;
	}

	public double getY() {
		return y;
	}

	public double setY(double y) {
		this.y = y;
		return y;
	}

	public int y() {
		return (int) y;
	}
	
	public Location add(Vector vector) {
		this.x += vector.getX();
		this.y += vector.getY();
		return this;
	}
	
	public Location add(double x, double y) {
		this.x += x;
		this.y += y;
		return this;
	}
	
	@Override
	public Location clone() {
		return new Location(arena, x, y);
	}

	@Override
	public String toString() {
		return "Location [arena=" + arena + ", width=" + x + ", height=" + y + "]";
	}
}
