package com.zettelnet.superbash.util;


public interface Positionable {

	public Location getLocation();
}
