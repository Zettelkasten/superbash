package com.zettelnet.superbash;

import java.awt.Image;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

public class ResourceManager {

	private final SuperBash game;

	private final Map<String, Image> images;

	public ResourceManager(final SuperBash game) {
		this.game = game;

		this.images = new HashMap<>();
	}

	public SuperBash getGame() {
		return game;
	}

	public boolean loadImage(String key, String res) {
		try {
			images.put(key, ImageIO.read(ClassLoader.getSystemResource(res)));
			return true;
		} catch (IOException e) {
			System.out.println(String.format("Failed to load resource %s from %s:", key, res));
			e.printStackTrace();
			return false;
		}
	}

	public Image getImage(String key) {
		return images.get(key);
	}
}
