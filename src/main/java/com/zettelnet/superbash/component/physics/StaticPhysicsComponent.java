package com.zettelnet.superbash.component.physics;

import com.zettelnet.superbash.entity.Component;
import com.zettelnet.superbash.entity.Entity;
import com.zettelnet.superbash.entity.EntityData;
import com.zettelnet.superbash.util.Vector;

public class StaticPhysicsComponent implements Component {

	private Vector velocity;
	
	public StaticPhysicsComponent(Vector velocity) {
		this.velocity = velocity;
	}
	
	@Override
	public void initialize(Entity entity) {
		entity.set(EntityData.VELOCITY, velocity);
	}

	@Override
	public void tick(Entity entity) {
		Vector velocity = entity.get(EntityData.VELOCITY, Vector.class);
		entity.getLocation().add(velocity);
	}
}
