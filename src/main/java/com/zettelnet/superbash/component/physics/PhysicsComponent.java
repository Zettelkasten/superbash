package com.zettelnet.superbash.component.physics;

import com.zettelnet.superbash.entity.Component;
import com.zettelnet.superbash.entity.Entity;
import com.zettelnet.superbash.entity.EntityData;
import com.zettelnet.superbash.util.Vector;

public class PhysicsComponent implements Component {

	@Override
	public void initialize(Entity entity) {
		entity.set(EntityData.VELOCITY, Vector.NULL);
		entity.set(EntityData.VELOCITY_HORIZONTAL, Vector.NULL);
		entity.set(EntityData.VELOCITY_JUMP, Vector.NULL);
	}

	@Override
	public void tick(Entity entity) {
		int airTime = entity.getInt(EntityData.AIR_TIME);

		Vector jump = entity.get(EntityData.VELOCITY_JUMP, Vector.class);
		Vector gravity = entity.getArena().getGravity().multiply(airTime);
		Vector horizontal = entity.get(EntityData.VELOCITY_HORIZONTAL, Vector.class);
		Vector velocity = jump.add(gravity).add(horizontal);

		entity.set(EntityData.VELOCITY, velocity);

		boolean onGround = entity.getBoolean(EntityData.ON_GROUND);
		if (onGround && velocity.getY() > 0) {
			velocity = new Vector(velocity.getX(), 0);
		}
		entity.getLocation().add(velocity);
	}
}
