package com.zettelnet.superbash.component.physics;

import com.zettelnet.superbash.entity.Component;
import com.zettelnet.superbash.entity.Entity;
import com.zettelnet.superbash.entity.EntityData;
import com.zettelnet.superbash.util.Box;
import com.zettelnet.superbash.util.Location;

public class RespawnWhenFarAwayComponent implements Component {

	private final Box limits;

	public RespawnWhenFarAwayComponent(Box limits) {
		this.limits = limits;
	}

	@Override
	public void initialize(Entity entity) {
		entity.set(EntityData.RESPAWN_BOUNDRY, limits);
	}

	@Override
	public void tick(Entity entity) {
		Box limits = entity.get(EntityData.RESPAWN_BOUNDRY, Box.class);
		
		if (!entity.overlapps(new Location(entity.getArena(), 0, 0), limits)) {
			entity.setLocation(entity.getArena().getSpawnLocation());
		}
	}
}
