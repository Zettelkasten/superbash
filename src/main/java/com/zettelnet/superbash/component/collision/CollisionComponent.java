package com.zettelnet.superbash.component.collision;

import java.awt.Color;
import java.util.Collection;

import com.zettelnet.superbash.entity.Component;
import com.zettelnet.superbash.entity.Entity;
import com.zettelnet.superbash.entity.EntityData;
import com.zettelnet.superbash.util.Box;
import com.zettelnet.superbash.util.Location;
import com.zettelnet.superbash.util.Vector;

public class CollisionComponent implements Component {

	private final Vector collisionOffset;
	private final Box collisionBox;

	public CollisionComponent() {
		this(Vector.NULL, null);
	}

	public CollisionComponent(Vector collisionOffset, Box collisionBox) {
		this.collisionOffset = collisionOffset;
		this.collisionBox = collisionBox;
	}

	@Override
	public void initialize(Entity entity) {
		entity.set(EntityData.COLLISION_OFFSET, collisionOffset);
		if (collisionBox == null) {
			entity.set(EntityData.COLLISION_BOX, entity.getBoundry());
		} else {
			entity.set(EntityData.COLLISION_BOX, collisionBox);
		}
	}

	@Override
	public void tick(Entity entity) {
		Vector velocity = entity.get(EntityData.VELOCITY, Vector.class);
		Vector collisionOffset = entity.get(EntityData.COLLISION_OFFSET, Vector.class);
		Box collisionBox = entity.get(EntityData.COLLISION_BOX, Box.class);

		boolean onGround = false;
		int airTime = entity.getInt(EntityData.AIR_TIME);
		Entity platform = null;

		Location platformLocation = entity.getLocation().clone().add(velocity).add(collisionOffset);
		Collection<Entity> bottomPlatforms = entity.getArena().getPlatform(platformLocation, collisionBox);

		for (Entity bottomPlatform : bottomPlatforms) {
			if (bottomPlatform.getLocation().getY() >= entity.getLocation().getY() + entity.getBoundry().getHeight()) {
				bottomPlatform.set(EntityData.HIGHLIGHT, Color.ORANGE);
				entity.getLocation().setY(bottomPlatform.getLocation().getY() - entity.getBoundry().getHeight());
				onGround = true;
				airTime = 0;
				platform = bottomPlatform;
				break;
			}
		}
		if (!onGround) {
			airTime++;
		}
		entity.set(EntityData.ON_GROUND, onGround);
		entity.set(EntityData.AIR_TIME, airTime);
		entity.set(EntityData.PLATFORM, platform);
	}
}
