package com.zettelnet.superbash.component.input;

import java.awt.event.KeyEvent;

import com.zettelnet.superbash.event.Event;
import com.zettelnet.superbash.event.HandlerList;

public class KeyInputEvent extends Event {

	private final static HandlerList handlers = new HandlerList();

	private final KeyEvent event;

	public KeyInputEvent(KeyEvent event) {
		this.event = event;
	}

	public KeyEvent getEvent() {
		return event;
	}

	public int getModifiers() {
		return event.getModifiers();
	}

	public Object getSource() {
		return event.getSource();
	}

	public boolean isAltDown() {
		return event.isAltDown();
	}

	public boolean isAltGraphDown() {
		return event.isAltGraphDown();
	}

	public boolean isControlDown() {
		return event.isControlDown();
	}

	public boolean isMetaDown() {
		return event.isMetaDown();
	}

	public boolean isShiftDown() {
		return event.isShiftDown();
	}

	public char getKeyChar() {
		return event.getKeyChar();
	}

	public int getKeyCode() {
		return event.getKeyCode();
	}

	public int getKeyLocation() {
		return event.getKeyLocation();
	}

	public boolean isActionKey() {
		return event.isActionKey();
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
}
