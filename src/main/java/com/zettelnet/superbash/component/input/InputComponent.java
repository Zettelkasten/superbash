package com.zettelnet.superbash.component.input;

import java.awt.event.KeyEvent;

import com.zettelnet.superbash.KeyInput;
import com.zettelnet.superbash.entity.Component;
import com.zettelnet.superbash.entity.Entity;
import com.zettelnet.superbash.entity.EntityData;
import com.zettelnet.superbash.event.EventHandler;
import com.zettelnet.superbash.event.EventListener;
import com.zettelnet.superbash.event.EventManager;
import com.zettelnet.superbash.util.Direction;
import com.zettelnet.superbash.util.Vector;

public class InputComponent implements Component, EventListener {

	private boolean doJump;

	public InputComponent(EventManager eventManager) {
		eventManager.registerObservers(this);

		this.doJump = false;
	}

	@Override
	public void initialize(Entity entity) {
		entity.set(EntityData.JUMPS, 0);
		entity.set(EntityData.JUMPS_MAX, 2);
		entity.set(EntityData.DIRECTION, Direction.LEFT);
	}

	@Override
	public void tick(Entity entity) {
		KeyInput keyListener = entity.getGame().getKeyListener();

		if (doJump) {
			int jumps = entity.getInt(EntityData.JUMPS);
			int maxJumps = entity.getInt(EntityData.JUMPS_MAX);
			if (jumps < maxJumps) {
				jumps++;
				entity.set(EntityData.JUMPS, jumps);
				entity.set(EntityData.VELOCITY_JUMP, new Vector(0, -8));
				entity.set(EntityData.AIR_TIME, 0);
			}
			doJump = false;
		} else if (entity.getBoolean(EntityData.ON_GROUND)) {
			entity.set(EntityData.JUMPS, 0);
			entity.set(EntityData.VELOCITY_JUMP, Vector.NULL);
		}
		if (keyListener.isKeyDown(KeyEvent.VK_A)) {
			entity.set(EntityData.VELOCITY_HORIZONTAL, new Vector(-3, 0));
			entity.set(EntityData.DIRECTION, Direction.LEFT);
		} else if (keyListener.isKeyDown(KeyEvent.VK_D)) {
			entity.set(EntityData.VELOCITY_HORIZONTAL, new Vector(3, 0));
			entity.set(EntityData.DIRECTION, Direction.RIGHT);
		} else {
			entity.set(EntityData.VELOCITY_HORIZONTAL, new Vector(0, 0));
		}

		if (keyListener.isKeyDown(KeyEvent.VK_S)) {
			Entity platform = entity.get(EntityData.PLATFORM, Entity.class);
			if (platform != null) {
				boolean solid = platform.getBoolean(EntityData.PLATFORM_SOLID);
				if (!solid) {
					entity.getLocation().add(0, 1);
				}
			}
		}
	}

	@EventHandler
	protected void onKeyInput(KeyInputEvent event) {
		if (event.getKeyChar() == 'w') {
			doJump = true;
		}
	}
}
