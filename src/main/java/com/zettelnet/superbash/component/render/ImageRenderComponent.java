package com.zettelnet.superbash.component.render;

import java.awt.Graphics;
import java.awt.Image;
import java.util.HashMap;
import java.util.Map;

import com.zettelnet.superbash.ResourceManager;
import com.zettelnet.superbash.entity.Entity;
import com.zettelnet.superbash.entity.EntityData;
import com.zettelnet.superbash.util.Direction;

public class ImageRenderComponent extends RenderComponent {

	private final Map<Direction, Image> images;

	public ImageRenderComponent(Map<Direction, Image> images) {
		this.images = images;
	}

	public ImageRenderComponent(ResourceManager res, String key) {
		this(getImages(res, key));
	}

	private static Map<Direction, Image> getImages(ResourceManager res, String key) {
		Image image = res.getImage(key);
		Map<Direction, Image> images = new HashMap<>();
		for (Direction dir : Direction.values()) {
			images.put(dir, image);
		}
		return images;
	}

	@Override
	public void initialize(Entity entity) {
	}

	@Override
	public void render(Entity entity, Graphics g) {
		Direction dir = entity.get(EntityData.DIRECTION, Direction.class);
		g.drawImage(images.get(dir), entity.getLocation().x(), entity.getLocation().y(), entity.getBoundry().width(),
				entity.getBoundry().height(), null);
	}
}
