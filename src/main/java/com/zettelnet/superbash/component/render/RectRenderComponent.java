package com.zettelnet.superbash.component.render;

import java.awt.Color;
import java.awt.Graphics;

import com.zettelnet.superbash.entity.Entity;

public class RectRenderComponent extends RenderComponent {

	private final Color color;
	private final boolean filled;

	public RectRenderComponent(Color color, boolean filled) {
		this.color = color;
		this.filled = filled;
	}

	@Override
	public void initialize(Entity entity) {
	}

	@Override
	public void render(Entity entity, Graphics g) {
		Color highlight = entity.get("highlight", Color.class);
		if (highlight != null) {
			g.setColor(highlight);
			entity.remove("highlight");
		} else {
			g.setColor(color);
		}

		if (filled) {
			g.fillRect(entity.getLocation().x(), entity.getLocation().y(), entity.getBoundry().width(), entity
					.getBoundry().height());
		} else {
			g.drawRect(entity.getLocation().x(), entity.getLocation().y(), entity.getBoundry().width(), entity
					.getBoundry().height());
		}
	}
}
