package com.zettelnet.superbash.component.render;

import java.awt.Graphics;

import com.zettelnet.superbash.entity.Component;
import com.zettelnet.superbash.entity.Entity;

public abstract class RenderComponent implements Component {

	@Override
	public void tick(Entity entity) {
		render(entity, entity.getGame().getGraphics());
	}

	public abstract void render(Entity entity, Graphics g);

}
