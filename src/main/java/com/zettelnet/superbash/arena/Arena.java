package com.zettelnet.superbash.arena;

import java.awt.Graphics;
import java.awt.Image;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.zettelnet.superbash.ResourceManager;
import com.zettelnet.superbash.SuperBash;
import com.zettelnet.superbash.component.collision.CollisionComponent;
import com.zettelnet.superbash.component.input.InputComponent;
import com.zettelnet.superbash.component.physics.PhysicsComponent;
import com.zettelnet.superbash.component.physics.RespawnWhenFarAwayComponent;
import com.zettelnet.superbash.component.physics.StaticPhysicsComponent;
import com.zettelnet.superbash.component.render.ImageRenderComponent;
import com.zettelnet.superbash.entity.Entity;
import com.zettelnet.superbash.entity.EntityBuilder;
import com.zettelnet.superbash.entity.EntityData;
import com.zettelnet.superbash.util.Box;
import com.zettelnet.superbash.util.Direction;
import com.zettelnet.superbash.util.Location;
import com.zettelnet.superbash.util.Sizable;
import com.zettelnet.superbash.util.Tickable;
import com.zettelnet.superbash.util.Vector;

public class Arena implements Tickable, Sizable {

	private final SuperBash game;

	private final Box boundry;

	private final Image background;

	private final List<Entity> players;
	private final Set<Entity> platforms;

	private final Set<Entity> clouds;

	private Vector gravity;
	private Location spawn;

	public Arena(final SuperBash game, Box boundry) {
		this.game = game;

		this.boundry = boundry;

		ResourceManager res = game.getResourceManager();
		this.background = res.getImage("arena.background");

		this.players = new ArrayList<>(4);
		this.platforms = new HashSet<>();

		this.clouds = new HashSet<>();

		this.gravity = new Vector(0, 0.3);
		this.spawn = new Location(this, 100, 0);

		Map<Direction, Image> playerImages = new EnumMap<>(Direction.class);
		playerImages.put(Direction.LEFT, res.getImage("player.left.0"));
		playerImages.put(Direction.RIGHT, res.getImage("player.right.0"));

		double playerScale = 2;
		Entity player = new EntityBuilder()
				.withLocation(spawn.clone())
				.withBoundry(20 * playerScale, 27 * playerScale)
				.withComponents(
						new ImageRenderComponent(playerImages),
						new PhysicsComponent(),
						new CollisionComponent(new Vector(8 * playerScale, 1 * playerScale), new Box(4 * playerScale,
								26 * playerScale)), new InputComponent(game.getEventManager()),
						new RespawnWhenFarAwayComponent(boundry)).build();
		players.add(player);

		EntityBuilder platform = new EntityBuilder();

		platforms.add(platform.clone().withLocation(this, 33, 270).withBoundry(581, 0)
				.withValue(EntityData.PLATFORM_SOLID, true).build()); // main
		platforms.add(platform.clone().withLocation(this, 104, 233).withBoundry(158, 10).build()); // left
		platforms.add(platform.clone().withLocation(this, 370, 167).withBoundry(164, 0).build()); // right

		EntityBuilder cloud = new EntityBuilder().withComponents(new CollisionComponent());

		clouds.add(cloud
				.clone()
				.withLocation(this, -100, 18)
				.withBoundry(194, 116)
				.withComponents(new ImageRenderComponent(res, "arena.cloud.0"),
						new StaticPhysicsComponent(new Vector(0.1, 0))).build());
		clouds.add(cloud
				.clone()
				.withLocation(this, 400, 103)
				.withBoundry(202 * 0.5, 121 * 0.5)
				.withComponents(new ImageRenderComponent(res, "arena.cloud.1"),
						new StaticPhysicsComponent(new Vector(-0.1, 0))).build());
	}

	public SuperBash getGame() {
		return game;
	}

	@Override
	public void tick() {
		Graphics g = game.getGraphics();
		g.drawImage(background, 0, 0, boundry.width(), boundry.height(), null);

		for (Entity cloud : clouds) {
			cloud.tick();
		}

		for (Entity platform : platforms) {
			platform.tick();
		}

		for (Entity player : players) {
			player.tick();
		}
	}

	public Vector getGravity() {
		return gravity;
	}

	public Collection<Entity> getPlatform(Location loc, Box boundry) {
		Collection<Entity> entities = new HashSet<>();
		for (Entity platform : platforms) {
			if (platform.overlapps(loc, boundry)) {
				entities.add(platform);
			}
		}
		return entities;
	}

	@Override
	public Box getBoundry() {
		return boundry;
	}

	public boolean overlapps(Entity entity) {
		return overlapps(entity.getLocation(), entity.getBoundry());
	}

	public boolean overlapps(Location loc, Box boundry) {
		return boundry.overlapps(new Location(this, 0, 0), loc, boundry);
	}

	public boolean onBounds(Location loc) {
		return boundry.onBounds(new Location(this, 0, 0), loc);
	}

	public Location getSpawnLocation() {
		return spawn.clone();
	}
}
