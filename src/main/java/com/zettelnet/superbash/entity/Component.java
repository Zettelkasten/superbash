package com.zettelnet.superbash.entity;


public interface Component {

	public void initialize(Entity entity);

	public void tick(Entity entity);
}
