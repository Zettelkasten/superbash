package com.zettelnet.superbash.entity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zettelnet.superbash.SuperBash;
import com.zettelnet.superbash.arena.Arena;
import com.zettelnet.superbash.util.Box;
import com.zettelnet.superbash.util.Location;

public class Entity {

	private final List<Component> components;

	private final Map<String, Object> values;

	private Location loc;
	private Box boundry;

	public Entity(Location loc, Box boundry, List<Component> components) {
		this.components = components;
		this.values = new HashMap<>();

		this.loc = loc;
		this.boundry = boundry;

		initialize();
	}

	public Object get(String key) {
		return values.get(key);
	}

	public <T> T get(String key, Class<T> clazz) {
		if (!has(key)) {
			return null;
		}
		return clazz.cast(values.get(key));
	}

	public boolean getBoolean(String key) {
		if (!has(key)) {
			return false;
		}
		return (boolean) values.get(key);
	}

	public int getInt(String key) {
		if (!has(key)) {
			return 0;
		}
		return (int) values.get(key);
	}

	public boolean has(String key) {
		return values.containsKey(key);
	}

	public Object set(String key, Object value) {
		Object oldValue = get(key);
		values.put(key, value);
		return oldValue;
	}

	public Object remove(String key) {
		return set(key, null);
	}

	public void initialize() {
		for (Component comp : components) {
			comp.initialize(this);
		}
	}

	public void tick() {
		for (Component comp : components) {
			comp.tick(this);
		}
	}

	public Location getLocation() {
		return loc;
	}
	
	public Location getCenterLocation() {
		return loc.clone().add(boundry.toVector().multiply(0.5));
	}

	public void setLocation(Location loc) {
		this.loc = loc;
	}

	public Box getBoundry() {
		return boundry;
	}

	public void setBoundry(Box boundry) {
		this.boundry = boundry;
	}

	public Arena getArena() {
		return loc.getArena();
	}

	public SuperBash getGame() {
		return loc.getArena().getGame();
	}

	public boolean overlapps(Entity other) {
		return overlapps(other.getLocation(), other.getBoundry());
	}

	public boolean overlapps(Location loc, Box boundry) {
		return this.getBoundry().overlapps(this.getLocation(), loc, boundry);
	}

	public boolean onBounds(Location loc) {
		return this.getBoundry().onBounds(this.getLocation(), loc);
	}
}
