package com.zettelnet.superbash.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zettelnet.superbash.arena.Arena;
import com.zettelnet.superbash.util.Box;
import com.zettelnet.superbash.util.Location;

public class EntityBuilder implements Cloneable {

	private Location loc;
	private Box boundry;

	private List<Component> components;
	private Map<String, Object> values;

	public EntityBuilder() {
		this.loc = null;
		this.boundry = null;
		this.components = new ArrayList<>();
		this.values = new HashMap<>();
	}

	public EntityBuilder withLocation(Location loc) {
		this.loc = loc;
		return this;
	}

	public EntityBuilder withLocation(Arena arena, double x, double y) {
		return withLocation(new Location(arena, x, y));
	}

	public EntityBuilder withBoundry(Box boundry) {
		this.boundry = boundry;
		return this;
	}

	public EntityBuilder withBoundry(double width, double height) {
		return withBoundry(new Box(width, height));
	}

	public EntityBuilder withComponent(Component component) {
		this.components.add(component);
		return this;
	}

	public EntityBuilder withComponents(Component... components) {
		this.components.addAll(Arrays.asList(components));
		return this;
	}

	public EntityBuilder withComponents(Collection<Component> components) {
		this.components.addAll(components);
		return this;
	}

	public EntityBuilder withValue(String key, Object value) {
		this.values.put(key, value);
		return this;
	}

	public Entity build() {
		Entity entity = new Entity(loc, boundry, components);
		for (Map.Entry<String, Object> entry : values.entrySet()) {
			entity.set(entry.getKey(), entry.getValue());
		}
		return entity;
	}

	@Override
	public EntityBuilder clone() {
		EntityBuilder other = new EntityBuilder();
		other.withLocation(loc);
		other.withBoundry(boundry);
		other.withComponents(components);
		return other;
	}
}
