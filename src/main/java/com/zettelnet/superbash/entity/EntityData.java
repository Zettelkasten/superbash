package com.zettelnet.superbash.entity;

public final class EntityData {

	public static final String AIR_TIME = "airTime";
	public static final String VELOCITY = "velocity";
	public static final String HIGHLIGHT = "highlight";

	public static final String COLLISION_OFFSET = "collision.offset";
	public static final String COLLISION_BOX = "collision.box";
	
	public static final String ON_GROUND = "onGround";
	public static final String PLATFORM = "platform";
	public static final String PLATFORM_SOLID = "platform.solid";

	public static final String JUMPS = "jumps";
	public static final String JUMPS_MAX = "jumps.max";

	public static final String VELOCITY_JUMP = "velocity.jump";
	public static final String VELOCITY_HORIZONTAL = "velocity.horizontal";
	
	public static final String DIRECTION = "direction";
	public static final String RESPAWN_BOUNDRY = "respawnBoundry";
}
