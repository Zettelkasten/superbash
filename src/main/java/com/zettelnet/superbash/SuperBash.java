package com.zettelnet.superbash;

import java.awt.Graphics;

import com.zettelnet.superbash.arena.Arena;
import com.zettelnet.superbash.event.EventManager;
import com.zettelnet.superbash.util.Box;

public class SuperBash {

	private final GameFrame frame;

	private final GameThread gameThread;

	private final KeyInput keyInput;

	private final ResourceManager resourceManager;
	private final EventManager eventManager;

	private Arena arena;

	private Graphics g;

	public SuperBash() {
		this.frame = new GameFrame(this, 640, 400);
		this.gameThread = new GameThread(this, 60);
		this.keyInput = new KeyInput(this);

		this.resourceManager = new ResourceManager(this);
		initialize();

		this.eventManager = new EventManager();

		this.frame.getCanvas().addKeyListener(keyInput);
	}

	public void start() {
		this.arena = new Arena(this, new Box(640, 400));

		this.gameThread.start();
	}

	public void initialize() {
		resourceManager.loadImage("arena.background", "background.png");
		resourceManager.loadImage("arena.cloud.0", "cloud.png");
		resourceManager.loadImage("arena.cloud.1", "cloud1.png");
		// resourceManager.loadImage("platform.texture.0", "platform0.png");
		resourceManager.loadImage("player.left.0", "steve-l.png");
		resourceManager.loadImage("player.right.0", "steve-r.png");
	}

	public void tick() {
		frame.render();
	}

	protected void performTick(Graphics g) {
		this.g = g;
		arena.tick();
		this.g = null;
	}

	public GameFrame getFrame() {
		return frame;
	}

	public ResourceManager getResourceManager() {
		return resourceManager;
	}

	public EventManager getEventManager() {
		return eventManager;
	}

	public Arena getArena() {
		return arena;
	}

	// public static synchronized void playSound(final String url) {
	// new Thread(new Runnable() {
	// public void run() {
	// try {
	// Clip clip = AudioSystem.getClip();
	// AudioInputStream inputStream =
	// AudioSystem.getAudioInputStream(Main.class.getResourceAsStream(url));
	// clip.open(inputStream);
	// clip.start();
	// } catch (Exception e) {
	// System.err.println(e.getMessage());
	// }
	// }
	// }).start();
	// }

	public Graphics getGraphics() {
		return g;
	}

	public KeyInput getKeyListener() {
		return keyInput;
	}
}
