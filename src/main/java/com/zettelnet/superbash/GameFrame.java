package com.zettelnet.superbash;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class GameFrame extends JFrame {
	private static final long serialVersionUID = 3836599275255833847L;

	private final SuperBash game;

	private final JPanel panel;
	private final Canvas canvas;
	private final BufferStrategy bufferStrategy;

	public GameFrame(final SuperBash game, int width, int height) {
		this.game = game;

		this.setTitle("Super Bash");
		this.setSize(width, height);
		this.panel = (JPanel) this.getContentPane();
		this.panel.setPreferredSize(new Dimension(width, height));
		this.panel.setLayout(null);

		this.canvas = new Canvas();
		this.canvas.setBounds(0, 0, width, height);
		this.canvas.setIgnoreRepaint(true);
		this.canvas.setSize(width, height);
		this.panel.add(canvas);
		this.panel.setBounds(0, 0, width, height);
		this.panel.setSize(width, height);
		this.setMenuBar(null);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		pack();
		setVisible(true);

		canvas.createBufferStrategy(2);
		bufferStrategy = canvas.getBufferStrategy();

		canvas.requestFocus();
	}

	public void render() {
		Graphics g = bufferStrategy.getDrawGraphics();
		g.clearRect(0, 0, getWidth(), getHeight());

		this.game.performTick(g);

		g.dispose();

		bufferStrategy.show();
	}

	public JPanel getPanel() {
		return panel;
	}

	public Canvas getCanvas() {
		return canvas;
	}
}
