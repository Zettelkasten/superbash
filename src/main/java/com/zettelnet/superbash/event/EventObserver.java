package com.zettelnet.superbash.event;

public interface EventObserver {

	public void onEvent(Event event);
}
