package com.zettelnet.superbash.event;

import com.zettelnet.superbash.entity.Entity;

public abstract class EntityEvent extends Event {

	private final Entity entity;

	public EntityEvent(final Entity entity) {
		this.entity = entity;
	}

	public Entity getEntity() {
		return entity;
	}
}
