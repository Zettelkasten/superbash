package com.zettelnet.superbash.event;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class EventManager {

	protected HandlerList getHandlerList(Class<? extends Event> eventType) {
		try {
			return (HandlerList) eventType.getMethod("getHandlerList").invoke(null);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
				| SecurityException e) {
			throw new RuntimeException(e);
		}
	}

	public void registerObserver(Class<? extends Event> eventType, EventObserver observer) {
		getHandlerList(eventType).registerObserver(observer);
	}

	public void registerObservers(final EventListener listener) {
		final Class<? extends EventListener> clazz = listener.getClass();
		for (final Method method : clazz.getDeclaredMethods()) {
			EventHandler annotation = method.getDeclaredAnnotation(EventHandler.class);
			if (annotation == null) {
				continue;
			}
			Class<?>[] parameters = method.getParameterTypes();
			if (parameters.length != 1 || parameters[0].isAssignableFrom(Event.class)) {
				throw new RuntimeException(String.format("Method %s has to have only one parameter extending Event"));
			}
			@SuppressWarnings("unchecked")
			Class<? extends Event> eventType = (Class<? extends Event>) parameters[0];
			registerObserver(eventType, new EventObserver() {
				@Override
				public void onEvent(Event event) {
					try {
						method.setAccessible(true);
						method.invoke(listener, event);
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						e.printStackTrace();
					}
				}
			});
		}
	}

	public void callEvent(Event event) {
		event.getHandlers().onEvent(event);
	}
}
