package com.zettelnet.superbash.event;

import java.util.ArrayList;
import java.util.List;

public class HandlerList {

	private final List<EventObserver> observerList;
	private EventObserver[] observers;

	private boolean baked;

	public HandlerList() {
		this.observerList = new ArrayList<>();
		this.observers = new EventObserver[0];
	}

	public void registerObserver(EventObserver observer) {
		observerList.add(observer);
		baked = false;
	}

	public void bake() {
		observers = observerList.toArray(new EventObserver[observerList.size()]);
		baked = true;
	}

	public void onEvent(Event event) {
		if (!baked) {
			bake();
		}
		for (EventObserver observer : observers) {
			observer.onEvent(event);
		}
	}
}
