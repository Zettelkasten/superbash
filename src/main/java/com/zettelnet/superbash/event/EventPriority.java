package com.zettelnet.superbash.event;

public enum EventPriority {

	LOWEST, LOW, NORMAL, HIGH, HIGHEST, MONITOR;
}
