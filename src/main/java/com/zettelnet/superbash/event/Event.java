package com.zettelnet.superbash.event;

public abstract class Event {

	public String getName() {
		return getClass().getSimpleName();
	}

	public void consume() {
	}

	public abstract HandlerList getHandlers();
}
