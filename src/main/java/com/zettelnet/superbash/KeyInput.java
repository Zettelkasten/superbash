package com.zettelnet.superbash;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashSet;
import java.util.Set;

import com.zettelnet.superbash.component.input.KeyInputEvent;

public class KeyInput implements KeyListener {

	private final SuperBash game;

	private final Set<Integer> pressedKeys;

	public KeyInput(final SuperBash game) {
		this.game = game;
		this.pressedKeys = new HashSet<>();
	}

	@Override
	public void keyPressed(KeyEvent event) {
		if (pressedKeys.add(event.getKeyCode())) {
			game.getEventManager().callEvent(new KeyInputEvent(event));
		}
		switch (event.getKeyCode()) {
		case KeyEvent.VK_W:
			break;
		case KeyEvent.VK_A:
			break;
		case KeyEvent.VK_D:
		}
	}

	@Override
	public void keyReleased(KeyEvent event) {
		pressedKeys.remove(event.getKeyCode());
	}

	@Override
	public void keyTyped(KeyEvent event) {
	}

	public SuperBash getGame() {
		return game;
	}

	public boolean isKeyDown(int key) {
		return pressedKeys.contains(key);
	}
}
