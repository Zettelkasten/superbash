package com.zettelnet.superbash;

public class GameThread extends Thread {

	private final SuperBash game;

	private int targetRate;
	private long millisPerFrame;

	private boolean running;

	public GameThread(final SuperBash game, int targetRate) {
		this.game = game;
		setTargetRate(targetRate);
	}

	@Override
	public void run() {
		if (this.running) {
			throw new IllegalStateException();
		}
		this.running = true;

		while (this.running) {
			try {
				Thread.sleep(millisPerFrame);
				this.game.tick();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public boolean isRunning() {
		return this.running;
	}

	public void setRunning(boolean running) {
		if (running && !this.running) {
			run();
		} else {
			this.running = false;
		}
	}

	public int getTargetRate() {
		return this.targetRate;
	}

	public void setTargetRate(int targetRate) {
		if (targetRate <= 0) {
			throw new IllegalArgumentException();
		}
		this.targetRate = targetRate;
		this.millisPerFrame = 1000L / targetRate;
	}
}
